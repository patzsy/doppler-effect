import React, { Component } from 'react';

import './style/styles.scss';
import InputController from './InputController';
import Paint from './Paint';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      velocity: 0,
      paintWidth: 420,
      paintHeight: 420,
      textInput: 0
    };
  }

  getLogarithmicValue = (position) => {
    if (position < 0) {
      position = Math.abs(position);
      // position will be between 0 and 100
      var minp = 0;
      var maxp = 100;

      // The result should be between 1 an 100
      var minv = Math.log(1);
      var maxv = Math.log(100);

      // calculate adjustment factor
      var scale = (maxv - minv) / (maxp - minp);
      // console.log(-1 * (Math.exp(minv + scale * (position - minp)).toFixed(1)));

      return -1 * (Math.exp(minv + scale * (position - minp)).toFixed(1));
    } else {
      return position;
    }
  }





  handleInputChange = (e) => {
    this.setState({
      velocity: e,
      textInput: this.getLogarithmicValue(e)
    })
  }
  // componentWillMount() {
    
  // }


  render() {
    return (
      <div className="wrapper">
        <div className="canvas">
          <Paint
            paintWidth={this.state.paintWidth}
            paintHeight={this.state.paintHeight}
            velocity={this.state.velocity}
            getLogarithmicValue={this.getLogarithmicValue}

          />
        </div>
        <InputController
          velocity={this.state.velocity}
          maxValue={100}
          minValue={-100}
          value={this.state.value}
          textInput={this.state.textInput}
          handleInputChange={this.handleInputChange}
          handleInputNumChange={this.handleInputNumChange}
          getLogarithmicValue={this.getLogarithmicValue}
          formatInput={this.formatInput}

        />
      </div>

    );
  }

}
export default App;
