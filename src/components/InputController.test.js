import React from 'react';
import ReactDOM from 'react-dom';
import InputController from './InputController';
import "../setupTests"
import { mount ,shallow } from 'enzyme';



it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<InputController />, div);
});

it("should match the snapshot", () => {
  const wrapper = shallow(<InputController />);
  expect(wrapper).toMatchSnapshot();
});

it('should render inputs', () => {
  const wrapper = shallow(<InputController />);
  const input = wrapper.find('#slide-input');
  expect(input.exists()).toBe(true);
});
it('should render inputs', () => {
  const wrapper = shallow(<InputController />);
  const input = wrapper.find('#range-input');
  expect(input.exists()).toBe(true);
});

