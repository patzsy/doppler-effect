import React from "react";

const InputController = (props) => {

  return (
    <div className="controller">
      <div className="controller__input">
        <label> Velocity (km/s)
          <input
            type="text"
            className="styled-input__input"
            id="slide-input"
            // value={props.getLogarithmicValue(props.velocity)}
            value={(props.textInput)}
            onChange={(e) => props.handleInputNumChange(e.target.value)}
            pattern="[0-9]+"
            // keyboardType = 'numeric'
            min={props.minValue}
            max={props.maxValue}
            onBlur={(e) => props.formatInput(e.target.value)}
          />
        </label>
      </div>
      <div className="controller__slider">
        <input
          className="styled-input__range"
          type="range"
          id="range-input"
          min={props.minValue}
          max={props.maxValue}
          value={props.velocity}
          onChange={(e) => props.handleInputChange(e.target.value)}
        />
      </div>
    </div>
  );
};

export default InputController;