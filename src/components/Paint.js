import React from "react";
import StarImage from '../assets/star-small.png';


class Paint extends React.Component {

  constructor(props) {
    super(props);
    this.canvasRef = React.createRef();
    this.state = {
      originalImgData: {},
    }
  }

  componentDidMount() {
    if (window.innerWidth < 420) {
      this.setState({ paintWidth: window.innerWidth, paintHeight: window.innerWidth });
    }
    const canvas = this.canvasRef.current;
    const ctx = canvas.getContext('2d');
    const img = new Image();
    img.src = `${StarImage}`;

    img.onload = () => {
      //resize image if it's bigger than canvas
      const scaleFactor = img.width / img.height;
      const maxWindowWidth = this.props.paintWidth;
      const maxWindowHeight = maxWindowWidth  * scaleFactor;
      var width = img.width;
      var height = img.height;
      if (width > maxWindowWidth) {
        width = maxWindowWidth;
      }  
      if (height > maxWindowHeight) {
         height = maxWindowHeight ;
      }
      ctx.drawImage(img, 0, 0, width, height);
      this.setState({
        originalImgData: ctx.getImageData(0, 0, canvas.width, canvas.height)
      })
    }
  }

  componentDidUpdate() {
    if (this.props.velocity !== 0) {
      this.rePaint(this.props.velocity);
    } else {
      this.rePaint(0);
    }
  }



  rePaint = (shift) => {
    // shift value calculation
    
    if (shift < 0) {
      shift = this.props.getLogarithmicValue(shift);
      shift = Math.abs(shift / 210).toFixed(4) * 1;
    } else if (shift > 0) {
      shift = Math.abs(shift / 1200).toFixed(4) * -1;
    } else {
      shift = 0;
    }

    const canvas = this.canvasRef.current;
    const ctx = canvas.getContext('2d');
    const original = this.state.originalImgData.data;
    const update = ctx.getImageData(0, 0, canvas.width, canvas.height);
    for (var i = 0; i < original.length; i += 4) {
      let red = original[i + 0];
      let green = original[i + 1];
      let blue = original[i + 2];
      let alpha = original[i + 3];

      // skip transparent/semiTransparent pixels
      if (alpha < 30) { continue; }
      // convert to HSL
      let hsl = this.rgbToHsl(red, green, blue);
      let newRgb;

      if (this.props.velocity < 0) {
        //shift to blue
        newRgb = this.hslToRgb(hsl.h + shift, hsl.s + shift, hsl.l);
      } else if (this.props.velocity > 0) {
        //shift to red
        newRgb = this.hslToRgb(hsl.h + shift, hsl.s + 0.05, hsl.l - 0.05);
      } else {
        newRgb = this.hslToRgb(hsl.h, hsl.s, hsl.l);
      }
      update.data[i + 0] = newRgb.r;
      update.data[i + 1] = newRgb.g;
      update.data[i + 2] = newRgb.b;
      update.data[i + 3] = alpha;
    }

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.putImageData(update, 0, 0);
  }

  rgbToHsl = (r, g, b) => {
    r /= 255; g /= 255; b /= 255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;
    if (max === min) {
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r: h = (g - b) / d + (g < b ? 6 : 0); break;
        case g: h = (b - r) / d + 2; break;
        case b: h = (r - g) / d + 4; break;
        default: return;
      }
      h /= 6;
    }
    return ({ h: h, s: s, l: l });
  }

  hslToRgb = (h, s, l) => {
    var r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      function hue2rgb(p, q, t) {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      }
      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      var p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    return ({
      r: Math.round(r * 255),
      g: Math.round(g * 255),
      b: Math.round(b * 255),
    });
  }

  render() {
    return (
      <div>
        <canvas
          className="canvas"
          ref={this.canvasRef}
          width={this.props.paintWidth}
          height={this.props.paintHeight}
        />
      </div>
    )
  }
}


export default Paint;