import React from 'react';
import ReactDOM from 'react-dom';
import Paint from './Paint';
import "../setupTests";


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Paint />, div);
});

 
window.HTMLCanvasElement.prototype.getContext = () => {};
