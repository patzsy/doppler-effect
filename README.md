# Doppler effect

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Tested on Chrome 79.0.3945.130, Safari 13.0.4, Firefox 72.0.2 (64-bit), iPhone 11 Pro(IOS 13.3).

## Getting started

``` bash
# install dependencies
> npm install
# serve with hot reload at localhost:3000
> npm run start
# or serve with production
> npm run build
> npm install -g serve
> serve -s build
```

